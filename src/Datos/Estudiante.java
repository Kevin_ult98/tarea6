package Datos;

public class Estudiante {

    private String cedula;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String telefono;



    public Estudiante() {
    }

    public Estudiante(String cedula, String nombre, String apellido1, String apellido2, String telefono) {

        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.telefono = telefono;
    }




    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String toStringArchivo(){
        return cedula + "@" + nombre + "@" + apellido1 + "@" + apellido2 ;
    }

    @Override
    public String toString() {
        return cedula+","+ nombre+","+apellido1+","+apellido2+","+telefono;
    }


}

