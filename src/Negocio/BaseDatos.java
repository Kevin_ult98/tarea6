package Negocio;

import Datos.Estudiante;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class BaseDatos {
    private BufferedWriter ingreso;
    public void abrirArchivo() {
        try {
            ingreso = new BufferedWriter(new FileWriter("Registro.txt"));
        } catch (IOException e) {
            System.err.println("Hubo un error al crear el archivo");
            e.printStackTrace();
        }
    }
    public void escribirArchivo(Estudiante estudiantes) {
        try {
            ingreso.write (String.valueOf(new FileWriter("Registro.txt")));
        } catch (IOException e) {
            System.err.print("Hubo un error al escribir en el archivo");
            System.out.println(  e );
            cerrarArchivo();
        }
    }

    public void cerrarArchivo() {
        try {
            if (ingreso != null) {
                ingreso.close();
            }
        } catch (IOException e) {
            System.err.println("Hubo un error al cerrar el archivo");
            System.out.println(e);;
        }
    }
}
