package Negocio;

import Datos.Estudiante;

import java.io.*;

public class Lectura {
    BufferedReader leer;

    public void abrirArchivo() {
        try {
            leer = new BufferedReader( new FileReader( "Registro.txt" ) );
        } catch (IOException e) {
            System.err.println( "Hubo un error al crear el archivo" );
            e.printStackTrace();
        }
    }

    public Estudiante[] leerArchivo() throws IOException {
        Estudiante[] vectorEstudiantes = new Estudiante[100];
        Estudiante estudiantes;
        try {
            leer = new BufferedReader( new FileReader( "Registro.txt" ) );
            String linea = leer.readLine();
            String[] posicion;
            int indice;
            int contador = 0;
            while (linea != null) {
                estudiantes = new Estudiante();
                indice = 0;
                posicion = linea.split( "@" );
                estudiantes.setCedula( posicion[indice++] );
                estudiantes.setNombre( posicion[indice++] );
                estudiantes.setApellido1( posicion[indice++] );
                estudiantes.setApellido2( posicion[indice++] );
                estudiantes.setTelefono( posicion[indice++] );

                vectorEstudiantes[contador] = estudiantes;
                contador++;
                linea = leer.readLine();
            }
        } catch (FileNotFoundException e) {
            System.err.println( "Hubo un error al leer el archivo" );
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println( "Hubo un error al leer el archivo" );
            e.printStackTrace();
        }
        return vectorEstudiantes;
    }

    public int getLineasArchivoPlantilla() {
        int cantidadLineas = 0;
        try {
            leer = new BufferedReader( new FileReader( "Registro.txt" ) );
            String linea = leer.readLine();
            while (linea != null) {
                cantidadLineas++;
                linea = leer.readLine();
            }
        } catch (IOException e) {
            System.err.println( "Hubo un error al leer el archivo" );
            e.printStackTrace();
        }
        return (cantidadLineas);
    }

    public void cerrarArchivo() {
        try {
            if (leer != null) {
                leer.close();
            }
        } catch (IOException e) {
            System.err.println( "Hubo un error al cerrar el archivo" );
            e.printStackTrace();
        }
    }

        }


