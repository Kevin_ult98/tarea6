package Presentacion;

import Datos.Estudiante;
import Negocio.BaseDatos;
import Negocio.ManejadorVector;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IngresoUsuarios {
    private JPanel panel1;
    private JLabel lbCedula;
    private JLabel lblNombre;
    private JLabel lblApellido1;
    private JLabel lblApellido2;
    private JLabel lbltelefono;
    private JButton btnIngresar;
    private JButton btnRegresar;
    private JTextField CedulaT;
    private JTextField NombreT;
    private JTextField Apellido1T;
    private JTextField Apellido2T;
    private JTextField NumeroT;


    public IngresoUsuarios() {
        JFrame frame = new JFrame( "Informacion" );
        frame.setContentPane( panel1 );
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.setSize( 400, 450 );
        frame.setResizable( false );
        frame.setLocationRelativeTo( null );
        frame.setVisible( true );
        lbCedula.setBounds( 40, 10, 150, 100 );
        lblNombre.setBounds( 40, 60, 150, 100 );
        lblApellido1.setBounds( 40, 110, 150, 100 );
        lblApellido2.setBounds( 40, 160, 150, 100 );
        lbltelefono.setBounds( 40, 210, 150, 100 );
        btnIngresar.setBounds( 70, 340, 100, 50 );
        btnRegresar.setBounds( 200, 340, 100, 50 );
        CedulaT.setBounds( 200, 50, 150, 20 );
        NombreT.setBounds( 200, 100, 150, 20 );
        Apellido1T.setBounds( 200, 150, 150, 20 );
        Apellido2T.setBounds( 200, 200, 150, 20 );
        NumeroT.setBounds( 200, 250, 150, 20 );
        panel1.setVisible( true );
        panel1.setLayout( null );

        //añadir más estudiantes al archivo
        btnIngresar.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Registro de Usuario
                String cedula = CedulaT.getText();
                String nombre = NombreT.getText();
                String apellido1 = Apellido1T.getText();
                String apellido2 = Apellido2T.getText();
                String numero = NumeroT.getText();
                ManejadorVector.vectorE[ManejadorVector.contador] = new Estudiante( cedula, nombre, apellido1, apellido2, numero );
                JOptionPane.showMessageDialog(null,"Estudiante Agregado Correctamente");
                ManejadorVector.contador++;
                  }


        } );
        btnRegresar.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                Login ventanaL = new Login();
            }
        } );
    }


}
